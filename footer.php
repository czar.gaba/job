

<footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="#">About</a>
              </li>
              <li class="list-inline-item">⋅</li>
              <li class="list-inline-item">
                <a href="#">Contact</a>
              </li>
              <li class="list-inline-item">⋅</li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
              <li class="list-inline-item">⋅</li>
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">© JOB <?php echo date('Y');?>. All Rights Reserved.</p>
          </div>
        </div>
      </div>
    </footer>

<script src="js/js.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="tinymce/tinymce.min.js"></script>
<script src="js/app.js"></script>
<script>tinymce.init({ 
    selector: '.mce',
    setup: function (editor) {
        editor.on('change', function (e) {
            editor.save();
        });
    }
});</script>
</body>
</html>