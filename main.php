
<?php include('header.php');checksetuser();?>
<?php 
  //
  $u=$_SESSION['USER'];
  $str= "select * from tbl_user where USERNAME=:u or EMAIL=:u";
  $cm= $conn->prepare($str);
  $cm->bindParam(':u', $u);
  
  $cm->execute();
  $user = $cm->rowcount();
  
  if ($user == 0) {
                  
  }else{
      while($row=$cm->fetch(PDO::FETCH_ASSOC)){
          $_SESSION['ID']=$row['ID'];
          $_SESSION['NAME']=$row['NAME'];
          $_SESSION['ACCESS']=$row['ACCESS'];
          $_SESSION['USERNAME']=$row['USERNAME'];
          $_SESSION['PASSWORD']=$row['PASSWORD'];
          $_SESSION['EMAIL']=$row['EMAIL'];
          $_SESSION['ADDRESS']=$row['ADDRESS'];
      }
  }
  //
?>


<?php 
if(isset($_GET['jobid'])){
    $st="update tbl_job set STATUS='closed' where ID=:i";
    $cmxx=$conn->prepare($st);
    $cmxx->bindvalue(':i', $_GET['jobid']);
    $cmxx->execute();
}

if($_SERVER['REQUEST_METHOD']=="POST"){

//

if(isset($_POST['addjob'])){
    $employer = $_SESSION['NAME'].$_SESSION['ID'];
$job_title= $_POST['job_title'];
$job_type= $_POST['job_type'];
$status="open";
$description=$_POST['desc'];
$date = date('Y-m-d');
$amount = $_POST['amount'];
$category = $_POST['category'];
    $str= "INSERT INTO `tbl_job`(`EMPLOYER`, `JOB_TITLE`, `JOB_TYPE`, `STATUS`, `DESCRIPTION`, `DATE`, `AMOUNT`, `CATEGORY`) VALUES (:e,:jt,:jty,:stat,:de,:dd,:am,:cat)";
		$cm=$conn->prepare($str);
        $cm->bindvalue(':e', $employer);
        $cm->bindvalue(':jt', $job_title);
        $cm->bindvalue(':jty', $job_type);
        $cm->bindvalue(':stat', $status);
        $cm->bindvalue(':de', $description);
        $cm->bindvalue(':dd', $date);
        $cm->bindvalue(':am', $amount);
        $cm->bindvalue(':cat', $category);
        

		if ($cm->execute()){
			echo "<script> alert('SUCCESS'); </script>";
		}else{
            echo "<script> alert('FAILED'); </script>";
        }
    }
//


    //
}
?>

<body class="<?php echo basename(__FILE__, '.php');?>">
<?php include('includes/nav.php');?>


<?php 
if($_SESSION['ACCESS']=="freelance"){
?>
    <div class="section1 space2">
        <div class="container">
            
            <div class="row">
            <div class="col-md-12">
                <h1>My applied jobs</h1>
            </div>
                <div class="col-md-12">

                <div id="accordion">


            <?php 
            $st = "select * from tbl_apply where FREELANCE_ID=:u order by AID desc";
            $cm=$conn->prepare($st);
            $cm->bindvalue(':u', $_SESSION['ID']);
            $cm->execute();
            while($row=$cm->fetch(PDO::FETCH_ASSOC)){

                $st = "select * from tbl_job where ID=:u";
                $cmx=$conn->prepare($st);
                $cmx->bindvalue(':u', $row['JOB_ID']);
                $cmx->execute();
                while($rowx=$cmx->fetch(PDO::FETCH_ASSOC)){
                        $title=$rowx['JOB_TITLE'];
                        $desc=$rowx['DESCRIPTION'];
                }



                ?>
                
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#x<?php echo $row['AID'];?>" aria-expanded="false" aria-controls="collapseOne">
          <?php echo $title;?>
        </button>
      </h5>
    </div>

    <div id="x<?php  echo $row['AID'];?>" class="collapse collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
       <?php echo $desc;?>
      </div>
    </div>
  </div>
  
                <?php
            }
            ?>


  
</div>

                <!-- end -->
                </div>
            </div>
        </div>        
    </div>
<?php
//employer
}elseif($_SESSION['ACCESS']=="employer"){

?>

<section class="space" style="background:#eee;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 space">
                <h2>Jobs Posted</h2>
            </div>
            

                <?php
                    $st = "select * from tbl_job where EMPLOYER=:e and STATUS='open'";
                    $cm=$conn->prepare($st);
                    $cm->bindvalue(':e', $_SESSION['NAME'].$_SESSION['ID']);
                    $cm->execute();
                    while($row=$cm->fetch(PDO::FETCH_ASSOC)){
                        ?>
                        
                            <div class="card col-md-4" style="">
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $row['JOB_TITLE'];?></h5>
                                <h6 class="card-subtitle mb-2 text-muted"><?php echo $row['JOB_TYPE'];?> - <?php echo $row['AMOUNT'];?></h6>
                                <p class="card-text"><?php echo fn_limit($row['DESCRIPTION'],100);?>  </p>
                                <a href="?jobid=<?php echo $row['ID'];?>" class="btn btn-success">CLOSE/HIRE</a>
                                <!-- <a href="#" class="btn btn-info">Update</a> -->
                                <!-- <a href="#" class="card-link btn-danger btn">Delete</a> -->
                                <a class="btn btn-dark" href="viewjob.php?jobid=<?php echo $row['ID'];?>" style="color:#fff;">View Job</a>
                            </div>
                            </div>
                        <?php
                    }
                ?>
                
            
        </div>
    </div>
</section>
<section class="space2">
    <div class="container">
        <div class="col-md-12">
            <h2>ADD JOB</h2>
            <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
            <div class="form-group">
                <label for="exampleInputEmail1">JOB TITLE</label>
                <input type="text" name="job_title" class="form-control" required>
               
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">AMOUNT</label>
                <input type="text" class="form-control" name="amount" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">JOB TYPE</label>
                <select class="form-control" name="job_type">
                    <option value="full time">Full Time</option>
                    <option value="part time">Part Time</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">CATEGORY</label>
                <select name="category" class="form-control">
                <?php
                $st="select * from tbl_job_category";
                $cm=$conn->prepare($st);
                $cm->execute();
                while($row=$cm->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <option value="<?php echo $row['ID'];?>"> <?php echo $row['CATEGORY'];?></option>
                    <?php
                }
                ?>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">DESCRIPTION</label>
                <textarea name="desc" class="mce" cols="30" rows="10" required></textarea>
            </div>
            
            

            
            <input type="hidden" name="addjob" value="addjob">
            <button type="submit"   class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</section>
<?php
}else{
    die();
}
?>


<?php include('footer.php');?>
