
$(document).ready(function(){

    $("#skill").hide();
    $("#cselect").change(function() {
        var val = $(this).val();
        if(val === "freelance") {
            $("#skill").show();
            $("#skill textarea").attr("required", "true");
        }
        else if(val === "employer") {
            $("#skill textarea").attr("required", "false");
            $("#skill").hide();
        }
      });

});