<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

try {
    $constring = 'mysql:host=localhost;dbname=job'; // host and db
    $user= 'root'; // define the username
    $pass=''; // password
    
    $conn = new PDO($constring, $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
       
    } catch (PDOException $e) {
        
        $msg = $e -> getmessage();
        echo "ERROR LIST ->".$msg;
        die();
    }

function checksetuser(){
    if(!isset($_SESSION['USER'])){
        die();
    }
    //
}
function fn_limit($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}
