-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2019 at 09:53 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `job`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_apply`
--

CREATE TABLE `tbl_apply` (
  `JOB_ID` text NOT NULL,
  `FREELANCE_ID` text NOT NULL,
  `AID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_apply`
--

INSERT INTO `tbl_apply` (`JOB_ID`, `FREELANCE_ID`, `AID`) VALUES
('2', '1', 6),
('5', '1', 8);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job`
--

CREATE TABLE `tbl_job` (
  `ID` int(11) NOT NULL,
  `EMPLOYER` text NOT NULL,
  `JOB_TITLE` text NOT NULL,
  `JOB_TYPE` text NOT NULL,
  `STATUS` text NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE` text NOT NULL,
  `AMOUNT` text NOT NULL,
  `CATEGORY` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_job`
--

INSERT INTO `tbl_job` (`ID`, `EMPLOYER`, `JOB_TITLE`, `JOB_TYPE`, `STATUS`, `DESCRIPTION`, `DATE`, `AMOUNT`, `CATEGORY`) VALUES
(2, 'NAME2', 'web ev wefws', 'full time', 'closed', 'test test test test', '2019-01-16', '$235', '6'),
(5, 'NAME2', 'english', 'full time', 'open', 'test', '2019-01-16', '$55', '2'),
(6, 'NAME2', 'testjob', 'full time', 'closed', '<p>test</p>', '2019-01-16', '$55', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_category`
--

CREATE TABLE `tbl_job_category` (
  `ID` int(11) NOT NULL,
  `CATEGORY` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_job_category`
--

INSERT INTO `tbl_job_category` (`ID`, `CATEGORY`) VALUES
(1, 'Office & Admin VA'),
(2, 'English'),
(3, 'Writing'),
(4, 'Marketing & Sales'),
(5, 'Advertising'),
(6, 'Web Development'),
(7, 'Web Master'),
(8, 'Graphics & Multimedia'),
(9, 'Software Development'),
(10, 'Finance & Management'),
(11, 'Customer Service'),
(12, 'Professional Service'),
(13, 'Project Management'),
(14, 'CMS Web'),
(15, 'Mobile App Development'),
(16, 'Social Media');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `ID` int(11) NOT NULL,
  `NAME` text NOT NULL,
  `ACCESS` text NOT NULL,
  `USERNAME` text NOT NULL,
  `PASSWORD` text NOT NULL,
  `EMAIL` text NOT NULL,
  `ADDRESS` text NOT NULL,
  `CONTACT` text NOT NULL,
  `SKILLS` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`ID`, `NAME`, `ACCESS`, `USERNAME`, `PASSWORD`, `EMAIL`, `ADDRESS`, `CONTACT`, `SKILLS`) VALUES
(1, 'NAME2', 'freelance', 'freelance', 'freelance', 'freelance@e.com', 'freelance', '123', ''),
(2, 'NAME', 'employer', 'employer', 'employer', 'employer@e.com', 'employer', '456', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_apply`
--
ALTER TABLE `tbl_apply`
  ADD PRIMARY KEY (`AID`);

--
-- Indexes for table `tbl_job`
--
ALTER TABLE `tbl_job`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_job_category`
--
ALTER TABLE `tbl_job_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_apply`
--
ALTER TABLE `tbl_apply`
  MODIFY `AID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_job`
--
ALTER TABLE `tbl_job`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_job_category`
--
ALTER TABLE `tbl_job_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
