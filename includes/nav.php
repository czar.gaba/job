<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="./">JOB</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="nav navbar-nav ml-auto">
          
          <li class="nav-item">
            <?php 
              if(isset($_SESSION['USER'])){
                ?>
                <a href="main.php" class="btn custom-btn btn-primary">Main Dashboard</a>
                <a href="#" class="btn btn-danger custom-btn"><?php echo $_SESSION['ACCESS'];?></a>
                  <a href="#" class="btn btn-info custom-btn"><?php echo $_SESSION['NAME']." ";?><i class="fa fa-wrench"></i></a>
                  <a class=" btn custom-btn btn-warning" href="logout.php">logout</a>
                <?php
              }else{
                ?>
                  <a class=" btn custom-btn btn-warning" href="login.php">LOGIN</a>
                  <a class=" btn custom-btn btn-primary" href="register.php">REGISTER</a>
                <?php
              }
            ?>
            
          </li>
         </ul>
      </div>
</nav>