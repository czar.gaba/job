
<?php include('header.php');?>
<body class="<?php echo basename(__FILE__, '.php');?>">
<?php include('includes/nav.php');?>

<?php if(!isset($_GET['jobid'])){
die();
}
//$st = "select * from tbl_job where  STATUS='open' and CATEGORY=:e ";
?>



<section class="space2">
    <div class="container">
        



    <div class="row">
          

        <?php
                    $st = "select * from tbl_job where ID=:e limit 1 ";
                    $cmx=$conn->prepare($st);
                    $cmx->bindvalue(':e', $_GET['jobid']);
                    $cmx->execute();
                    while($row=$cmx->fetch(PDO::FETCH_ASSOC)){
                        //preg_replace('#[0-9 ]*#', '', $words)
                        ?>
                        
                        <div class="col-md-12">
                            <div class="jumbotron">
                            <h1 class="display-4"><?php echo $row['JOB_TITLE'];?></h1>
                            <p class="lead"><?php echo $row['DESCRIPTION'];?></p>
                            <hr class="my-4">
                            <p><span>Job Type: </span><?php echo $row['JOB_TYPE'];?></p>
                            <p><span>SALARY: </span><?php echo $row['AMOUNT'];?></p>
                            <p><span>DATE POSTED: </span><?php echo $row['DATE'];?></p>
                            <?php $n = preg_replace('#[0-9 ]*#', '', $row['EMPLOYER']) ?>
                            <p><span>EMPLOYER NAME: </span><?php echo $n ;?></p>
                            
                            <?php 
                            
                                if(isset($_SESSION['ACCESS'])):
                                if ($_SESSION['ACCESS']=="freelance"){

                                    $str= "SELECT * FROM `tbl_apply` where  JOB_ID=:u and FREELANCE_ID=:p";
                                    $cm=$conn->prepare($str);
                                    $cm->bindvalue(':u', $_GET['jobid']);
		                            $cm->bindvalue(':p', $_SESSION['ID']);
                                    $cm->execute();
                                    $user = $cm->rowcount();
                                    
                                    if ($user != 0) {
                                        ?>
                                        <p class="lead">
                                        you already applied to this job     
                                        </p>
                                        <?php
                                        
                                    }else{
                                    ?>
                                    <p class="lead">
                                            <a class="btn btn-primary btn-lg" href="viewjob.php?jobid=<?php echo $row['ID'];?>&freelanceid=<?php echo $_SESSION['ID'];?>" role="button">apply</a>
                                        </p>
                                    <?php
                                    }

                                    }else{

                                        ?>
                                        <p class="lead">
                                        applicants: <br>    
                                        </p>
                                        <?php

                                        $strx= "SELECT * from tbl_user,tbl_apply where  tbl_apply.JOB_ID=:u  and tbl_user.ACCESS='freelance'";
                                        $cmxx=$conn->prepare($strx);
                                        $cmxx->bindvalue(':u', $_GET['jobid']);
                                        //$cmxx->bindvalue(':p', $_SESSION['ACCESS']);
                                        $cmxx->execute();
                                        while($rowxx=$cmxx->fetch(PDO::FETCH_ASSOC)){
                                        
                                        ?>
                                            <a style="margin-right:5px;" href="profile.php?profileid=<?php echo $rowxx['ID'];?>" class="btn btn-dark"><?php echo $rowxx['NAME'];?></a>
                                        <?php
                                        }
                                    }
                                endif;
                            ?>
                            
                            </div>


                            
                        </div>
                        <?php
                    }
                ?>


        
    </div>


        
    </div>
</section>

<?php 
if(isset($_GET['freelanceid'])){
    $str= "INSERT INTO `tbl_apply`(`JOB_ID`, `FREELANCE_ID`) VALUES (:u,:p)";
		$cm=$conn->prepare($str);
		$cm->bindvalue(':u', $_GET['jobid']);
		$cm->bindvalue(':p', $_GET['freelanceid']);
		
		if ($cm->execute()){
			echo "<script> alert('applied'); </script>";
			header("location:./");
		}
}
?>


<?php include('footer.php');?>
