
<?php include('header.php');?>
<body class="<?php echo basename(__FILE__, '.php');?>">
<?php include('includes/nav.php');?>

<section class="space2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>TITLE</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium error impedit maiores eum consequatur praesentium sequi tenetur laboriosam quo nostrum sunt ipsum voluptatum ex atque pariatur, alias quibusdam! Aspernatur, sint.</p>
            </div>
        </div>
    </div>
</section>


<!--
Office & Admin VA
English
Writing
Marketing & Sales
Advertising
Web Development
Web Master
Graphics & Multimedia
Software Development
Finance & Management
Customer Service
Professional Service
Project Management
CMS Web
Mobile App Development
Social Media


-->
<section class="current-jobs-wrap space">
        <div class="container">
            <h2 class="section-title text-center">Current Job Posts</h2>
            <div class="row"> 
            <?php
              $st="select * from tbl_job_category";
              $cm=$conn->prepare($st);
              $cm->execute();
              while($row=$cm->fetch(PDO::FETCH_ASSOC)){
                ?>
                  <div class="col-md-4" style="margin-bottom:10px;">
                    <a href="view.php?catid=<?php echo $row['ID'];?>" class="btn btn-custom btn-info" >
                     <?php echo $row['CATEGORY'];?>
                    </a>
                  </div>
                <?php
              }
            ?>
            </div>
        </div>
    </section>



<section class="features-icons bg-light text-center">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-screen-desktop m-auto text-primary"></i>
              </div>
              <h3>Title</h3>
              <p class="lead mb-0">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Facilis magni aliquam, similique soluta consequuntur labore, impedit repellat tenetur in ab assumenda at. Eligendi ipsam soluta praesentium. Modi officiis odio obcaecati.</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-layers m-auto text-primary"></i>
              </div>
              <h3>Title</h3>
              <p class="lead mb-0">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Facilis magni aliquam, similique soluta consequuntur labore, impedit repellat tenetur in ab assumenda at. Eligendi ipsam soluta praesentium. Modi officiis odio obcaecati.</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-check m-auto text-primary"></i>
              </div>
              <h3>Title</h3>
              <p class="lead mb-0">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Facilis magni aliquam, similique soluta consequuntur labore, impedit repellat tenetur in ab assumenda at. Eligendi ipsam soluta praesentium. Modi officiis odio obcaecati.</p>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section class="showcase">
      <div class="container-fluid p-0">
        <div class="row no-gutters">

          <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('images/bg-showcase-1.jpg');"></div>
          <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>Fully Responsive Design</h2>
            <p class="lead mb-0">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo animi quidem iusto dolorem eaque blanditiis delectus quae, in magni, quaerat vitae asperiores, soluta nihil nam! Ut ducimus in impedit architecto.</p>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-6 text-white showcase-img" style="background-image: url('images/bg-showcase-2.jpg');"></div>
          <div class="col-lg-6 my-auto showcase-text">
            <h2>Bootstrap 4</h2>
            <p class="lead mb-0">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vero, tempore soluta ipsum voluptatibus officiis perferendis aliquam nostrum sequi aspernatur porro similique suscipit eos magni quia ratione esse accusamus doloremque. Reprehenderit!</p>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('images/bg-showcase-3.jpg');"></div>
          <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>title &amp; title</h2>
            <p class="lead mb-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem rem nisi quam totam laborum error, explicabo consequatur? Dolor perspiciatis quo earum numquam fugiat impedit laboriosam, quasi, error eos, suscipit natus?</p>
          </div>
        </div>
      </div>
    </section>


    <section class="testimonials text-center bg-light">
      <div class="container">
        <h2 class="mb-5">What people are saying...</h2>
        <div class="row">
          <div class="col-lg-4">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <img class="img-fluid rounded-circle mb-3" src="images/testimonials-1.jpg" alt="">
              <h5>name</h5>
              <p class="font-weight-light mb-0">"This is fantastic! Thanks so much guys!"</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <img class="img-fluid rounded-circle mb-3" src="images/testimonials-1.jpg" alt="">
              <h5>name</h5>
              <p class="font-weight-light mb-0">"Bootstrap is amazing. I've been using it to create lots of super nice landing pages."</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
              <img class="img-fluid rounded-circle mb-3" src="images/testimonials-1.jpg" alt="">
              <h5>name</h5>
              <p class="font-weight-light mb-0">"Thanks so much for making these free resources available to us!"</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="bg-purple home-colored-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="content-wrap">
                        <h2 class="section-title txt-w">Why look to the Job?</h2>
                        <p class="fs-18 txt-w">Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime officiis quae sed expedita. Labore, at. Harum quaerat voluptatem qui fugiat iste quasi vitae sit facilis earum, eius doloremque ut omnis.</p>
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <img src="images/why-look-illus.png" alt="">
                </div>
            </div>
        </div>
    </section>


<?php include('footer.php');?>
